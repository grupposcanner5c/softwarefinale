﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace SoftwarePalestra
{
    /// <summary>
    /// Logica di interazione per WPFCorsi.xaml
    /// </summary>
    public partial class WPFCorsi : Window
    {
        public static bool isOpened = false;

        DBQueries database = new DBQueries("localhost", "root", null, "mypalestra");
        public WPFCorsi()
        {
            InitializeComponent();
            this.Show();
            isOpened = true;
            //SelezionaData.Format = DateTimeFormat.UniversalSortableDateTime;

        }

        private void InserisciLezione_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime date = SelezionaData.SelectedDate.Value;
                string durata = OraDurata.Text + ":" + MinutiDurata.Text + ":" + "00";
                string inizio = Ora.Text + ":" + Minuti.Text + ":" + "00";
                database.CreateLezione(database.GetIDCorso(SelezionaCorso.Text), date, inizio, durata);
                MainWindow main = (MainWindow)Application.Current.MainWindow;
                ((Entrata)main.entrate).ReloadCorsi();
                this.Close();
            }
            catch (Exception) { }
        }

        private void Creazione_del_Corso_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            isOpened = false;
        }

        private void CreaCorso_Click(object sender, RoutedEventArgs e)
        {
            if (TextBox_NuovoCorso.Text.Length != 0)
            {
                database.CreateCorso(TextBox_NuovoCorso.Text);
                this.Close();
            }
        }

        private void SelezionaCorso_Loaded(object sender, RoutedEventArgs e)
        {
            List<Corso> corsi = database.GetCorso();
            foreach (Corso corso in corsi)
            {
                SelezionaCorso.Items.Add(corso.Nome);
            }
        }

        private void Creazione_del_Corso_Closed(object sender, EventArgs e)
        {
            isOpened = false;
        }
    }

}
