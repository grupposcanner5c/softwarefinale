﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwarePalestra
{
    class DBQueries
    {
        private string server;
        private string userid;
        private string password;
        private string database;
        private string mySQLConnectionString;

        public DBQueries(string server, string userid, string password, string database)
        {
            //localhost
            this.server = server;
            //root
            this.userid = userid;
            //mypalestra
            this.database = database;
            //non c'è la password
            this.password = password;
            mySQLConnectionString = $"server={server};userid={userid};password={password};database={database};";

        }

        public DBQueries() : this("localhost", "root", "", "mypalestra")
        {

        }

        public List<Lezione> GetLezioni()
        {
            List<Lezione> lezioni = new List<Lezione>();
            string query = "SELECT * FROM lezione";
            DateTime giorno;
            string durata;
            string inizio;
            int id;
            using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
            {
                mysqlconnection.Open();
                using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 300;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                    //solo se è una query che riporta qualcosa
                    MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                    while (mysqldatareader.Read())
                    {
                        giorno = mysqldatareader.GetDateTime("giornoSettimanale");
                        durata = mysqldatareader.GetString("durata");
                        inizio = mysqldatareader.GetString("Inizio");
                        id = mysqldatareader.GetInt32("ID");
                        lezioni.Add(new Lezione(giorno, durata, id,inizio));
                    }
                }
            }

            return lezioni;
        }

        public List<Corso> GetCorso()
        {
            List<Corso> corsi = new List<Corso>();
            int id;
            string nome;
            string query = "SELECT * FROM corso";
            using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
            {
                mysqlconnection.Open();
                using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 300;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                    //solo se è una query che riporta qualcosa
                    MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                    mysqldatareader.Read();
                    //result = mysqldatareader.GetString(1);
                    do
                    {
                        id = mysqldatareader.GetInt32("ID");
                        nome = mysqldatareader.GetString("Nome");
                        corsi.Add(new Corso(id, nome));

                    } while (mysqldatareader.Read());
                }
            }

            return corsi;
        }
        public void DeleteCliente(int id)
        {
            string query = $"DELETE FROM cliente WHERE ID = {id};";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            finally
            {

            }
        }
        public void AddtoRegistro(int id)
        {
            string query = $"INSERT INTO registro SELECT Cliente.Nome,Cliente.Cognome,NOW(),Cliente.ID FROM cliente WHERE ID = {id};";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            finally
            {

            }
        }

        public int GetEntrate(int id)
        {
            int result=-1;
            string query = $"SELECT NEntrate FROM tessera join cliente on cliente.TesseraID = tessera.ID WHERE cliente.ID = {id};";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetInt32("NEntrate");

                    }
                    mysqlconnection.Close();
                }
            }
            finally
            {

            }
            return result;
        }
        public Boolean ReduceEntrate(int id)
        {
            Boolean result = false;
            int entrate = GetEntrate(id);
            if (entrate > 0)
            {
                result = true;
                string query = $"UPDATE tessera join cliente on cliente.TesseraID = tessera.ID SET NEntrate = NEntrate - 1 WHERE cliente.ID = {id};";
                try
                {
                    using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                    {
                        mysqlconnection.Open();
                        using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = 300; //In ms
                            cmd.CommandText = query;
                            MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                            mysqldatareader.Read();
                        }
                    }
                }
                finally
                {

                }
            }
            else { 
            
          
            }
            return result;
        }   

        public List<Accesso> GetAccessi(int ID)
        {
            List<Accesso> entrate = new List<Accesso>();
            string query = $"SELECT * FROM registro WHERE ID = {ID} ORDER BY Orario DESC;";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();

                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        //result = mysqldatareader.GetString(1);
                        do
                        {
                            entrate.Add(new Accesso(mysqldatareader.GetString("Nome"), mysqldatareader.GetString("Cognome"), mysqldatareader.GetDateTime("Orario"), mysqldatareader.GetInt32("ID")));

                        } while (mysqldatareader.Read());
                    }
                    mysqlconnection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return entrate;
        }

        public int GetMaxIDCorso()
        {
            int result;
            string query = $"SELECT MAX(corso.ID) AS ID FROM corso;";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetInt32("ID");
                    }
                }
            }
            finally
            {

            }

            return result;
        
        }
        public void CreateCorso(string nome)
        {
            string query = $"INSERT INTO corso (Nome,ID) VALUES ('{nome}',{GetMaxIDCorso()+1});";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                    mysqlconnection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public int CreateTessera(Boolean tipoTessera, int numeroEntrate)
        {
            //true nuova - false rinnovo
            string query = $"INSERT INTO tessera (tipoTessera,NEntrate) VALUES ({tipoTessera},{numeroEntrate});" +
                $"SELECT LAST_INSERT_ID() ID;";
            int retID = -1;
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        //cmd.ExecuteNonQuery();

                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        retID = mysqldatareader.GetInt32("ID");
                    }
                }
            }
            finally
            {

            }
            return retID;
        }

        public void CreatePartecipazione(int idUtente, int idLezione)
        {
            string query = $"INSERT INTO partecipa (cliente,corso) join Cliente on partecipa.Cliente=Cliente.ID, join Corso on partecipa.Corso=Corso.ID ({idUtente},{idLezione});";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            finally
            {

            }
        }

        public void DeleteTessera(int idTessera)
        {
            string query = $"DELETE FROM tessera WHERE ID = {idTessera});";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            finally
            {

            }
        }



        public void CreateLezione(int id, DateTime giornoSettimanale,string inizio,string durata)
        {
            string query = $"INSERT INTO lezione (ID,Inizio,giornoSettimanale,durata) VALUES ({id},'{inizio}','{giornoSettimanale.ToString("yyyy-MM-dd")}','{durata}')";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();

                    }
                }
            }
            finally
            {

            }

        }

        public Int32 GetIDCorso(string nomeCorso)
        {
            int result;
            string query = $"SELECT ID FROM corso WHERE Nome = \"{nomeCorso}\" ";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetInt32("ID");

                    }
                }
            }
            finally
            {

            }
            return result;
        }

        public string GetNome(int id)
        {
            string result;
            string query = $"SELECT Nome FROM cliente WHERE id = {id}";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta 
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetString("Nome");
                    }
                }
            }
            finally
            {

            }
            return result;
        }

        public void UpdateCliente(Cliente cliente)
        {
            string query = $"UPDATE cliente join tessera on cliente.TesseraID = tessera.ID SET Nome = \"{cliente.Nome}\", Cognome = \"{cliente.Cognome}\", Email = '{cliente.Email}', Telefono = '{cliente.Telefono}', Citta = '{cliente.Citta}', DataRegistrazione = \"{cliente.DataRegistrazione.ToString("yyyy-MM-dd HH:mm:ss.fff")}\", DataNascita = \"{cliente.DataNascita.ToString("yyyy-MM-dd HH:mm:ss.fff")}\", tessera.NEntrate = {cliente.Entrate}, Indirizzo = \"{cliente.Indirizzo}\", `Numero Civico` = \"{cliente.NCivico}\" WHERE cliente.ID = {cliente.ID}";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();

                    }
                }
            }
            finally
            {

            }
        }

        public void UpdateTessera(Tessera tessera)
        {
            string query = $"UPDATE tessera SET tipoTessera = {tessera.TipoTessera}, NEntrate = {tessera.NENTRATE} ";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();

                    }
                }
            }
            finally
            {

            }
        }

        public Cliente CreateCliente(int tesseraId, string nome, string cognome, string email, string telefono, string indirizzo, string ncivico, string citta, DateTime datanascita, DateTime dataregistrazione)
        {
            Console.WriteLine("Creo il cliente");
            Cliente cliente;
            string query = $"INSERT INTO cliente (TesseraID,Nome,Cognome,Email,Telefono,Indirizzo,`Numero Civico`,Citta,DataRegistrazione,DataNascita) VALUES ({tesseraId},\"{nome}\",\"{cognome}\",'{email}','{telefono}','{indirizzo}','{ncivico}','{citta}','{dataregistrazione.ToString("yyyy-MM-dd HH:mm:ss.fff")}','{datanascita.ToString("yyyy-MM-dd HH:mm:ss.fff")}');" +
                $"select last_insert_id() ID;";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        //cmd.ExecuteNonQuery();

                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();

                        cliente = new Cliente(mysqldatareader.GetInt32("ID"), tesseraId, nome, cognome, email, telefono, indirizzo, ncivico, citta, dataregistrazione, datanascita, 0);

                    }
                }
            }
            finally
            {

            }
            return cliente;
        }


        public string GetNomeCorso(int id = -1)
        {
            string query;
            string result = "";
            if (id == -1)
            {
                query = $"SELECT corso.Nome FROM corso JOIN lezione ON corso.ID = lezione.ID;";
            }
            else
            {
                query = $"SELECT corso.Nome FROM corso JOIN lezione ON corso.ID = lezione.ID WHERE corso.ID = {id};";
            }

            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta 
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetString("Nome");
                    }
                }
            }
            finally
            {

            }
            return result;
        }
        public Cliente GetCliente(int id)
        {
            Cliente result = new Cliente();
            string query = $"SELECT * FROM cliente inner join (select NEntrate, ID TesseraID from tessera) tmpTessera using (TesseraID) WHERE cliente.id = {id}";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che restituisce un valore
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result.ID = mysqldatareader.GetInt32("ID");
                        result.Nome = mysqldatareader.GetString("Nome");
                        result.Cognome = mysqldatareader.GetString("Cognome");
                        result.tesseraID = mysqldatareader.GetInt32("tesseraID");
                        result.Telefono = mysqldatareader.GetString("Telefono");
                        result.Indirizzo = mysqldatareader.GetString("Indirizzo");
                        result.NCivico = mysqldatareader.GetString("Numero Civico");
                        result.Citta = mysqldatareader.GetString("Citta");
                        result.Email = mysqldatareader.GetString("Email");
                        result.DataNascita = mysqldatareader.GetDateTime("DataNascita");
                        result.DataRegistrazione = mysqldatareader.GetDateTime("DataRegistrazione");
                        result.Entrate = mysqldatareader.GetInt32("NEntrate");
                    }
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                result = null;
            }
            return result;
        }


    }
    class Corso
    {
        private int id;
        private string nome;

        public Corso(int ID, string nome)
        {
            id = ID;
            this.nome = nome;
        }
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

    }

    class Lezione
    {
        private DateTime giornoSettimanale;
        private string durata;
        private string inizio;
        private int id;

        public Lezione(DateTime giorno, string timedurata, int id,string inizio)
        {
            giornoSettimanale = giorno;
            durata = timedurata;
            this.id = id;
            this.inizio = inizio;
        }
        public string GetNome()
        {
            DBQueries queries = new DBQueries("127.0.0.1", "root", "", "mypalestra");
            return queries.GetNomeCorso(this.id);
        }

        public string GetGiorno()
        {
            return giornoSettimanale.ToString("dd/MM/yyyy");
        }
        public DateTime giorno
        {
            get { return giornoSettimanale; }
            set { giornoSettimanale = value; }
        }

        public string Durata
        {
            get { return durata; }
            set { durata = value; }

        }

        public string Inizio
        {
            get { return inizio; }
            set { inizio = value; }
        }

    }

    class Cliente
    {
        private int id;
        private int tesseraId;
        private string nome;
        private string cognome;
        private string email;
        private string telefono;
        private string indirizzo;
        private string ncivico;
        private string citta;
        private DateTime dataregistrazione;
        private DateTime dataNascita;
        private int entrate;

        public Cliente(int id, int tesseraId, string nome, string cognome, string email, string telefono, string indirizzo, string ncivico, string citta, DateTime dataregistrazione, DateTime dataNascita, int entrate)
        {
            this.id = id;
            this.tesseraId = tesseraId;
            this.nome = nome;
            this.cognome = cognome;
            this.email = email;
            this.telefono = telefono;
            this.indirizzo = indirizzo;
            this.ncivico = ncivico;
            this.citta = citta;
            this.dataregistrazione = dataregistrazione;
            this.dataNascita = dataNascita;
            this.entrate = entrate;
        }

        public Cliente()
        {

        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public int tesseraID
        {
            get { return tesseraId; }
            set { tesseraId = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }
        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string NCivico
        {
            get { return ncivico; }
            set { ncivico = value; }
        }

        public string Citta
        {
            get { return citta; }
            set { citta = value; }
        }

        public DateTime DataRegistrazione
        {
            get { return dataregistrazione; }
            set { dataregistrazione = value; }
        }

        public DateTime DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = value; }
        }

        public int Entrate
        {
            get { return entrate; }
            set { entrate = value; }
        }
    }
    class Tessera
    {
        private int id;
        private int tipoTessera;
        private int NEntrate;

        public Tessera()
        { 
        
        }

        public Tessera(int id,int tipoTessera,int NEntrate)
        {
            this.id = id;
            this.tipoTessera = tipoTessera;
            this.NEntrate = NEntrate;
        
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public int TipoTessera
        {
            get { return tipoTessera; }
            set { tipoTessera = value; }
        }

        public int NENTRATE 
        {
            get { return NEntrate; }
            set { NEntrate = value; }
        }
    
    
    }
    class Accesso
    {
        private string nome;
        private string cognome;
        private DateTime data;
        private int id;

        public Accesso(string nome, string cognome, DateTime data, int ID)
        {
            this.nome = nome;
            this.cognome = cognome;
            this.data = data;
            this.id = ID;
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


    }
}
