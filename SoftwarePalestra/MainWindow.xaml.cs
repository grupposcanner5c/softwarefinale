﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SoftwarePalestra
{

    

    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool isNewUser;
        private MSRController controller;

        public  Window entrate;

        private string[] currentUsr;

        private bool isModify;

        public MainWindow()
        {
            ClearFields();

            ModifyTxtBox(false);

            this.isModify = false;
            this.isNewUser = false;

            controller = new MSRController("COM3");

            //btnStampa.Visibility = Visibility.Hidden;
            btnAggiungiACorso.Visibility = Visibility.Hidden;

            
        }

        private void LoadEntrate(MSRController controller)
        {
            Window wnd = new Entrata(controller);
            wnd.Show();
            System.Windows.Threading.Dispatcher.Run();
        }

        #region Button clicks

        private void btnModifica_Click(object sender, RoutedEventArgs e)
        {
            switch (isModify)
            {
                case true:
                    {
                        SaveModify();
                        ModifyTxtBox(false);
                        this.isModify = false;
                    }
                    break;

                case false:
                    {
                        ModifyTxtBox(true);
                        this.isModify = true;
                    }
                    break;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearFields();
        }

        private void BtnScansiona_Click(object sender, RoutedEventArgs e)
        {
            ClearFields();
            currentUsr = controller.AdminReadFromCard();
            UpdateFields();
        }

        private void BtnNuovoUtente_Click(object sender, RoutedEventArgs e)
        {
            ClearFields();
            isModify = true;
            isNewUser = true;
            ModifyTxtBox(true);
        }

        #endregion

        #region modifiche campi

        protected void ClearFields()
        {
            InitializeComponent();
            txtName.Clear();
            txtEmail.Clear();
            txtNtelefono.Clear();
            txtCognome.Clear();
            txtCitta.Clear();
            txtIndirizzo.Clear();
            dateRegistrazione.DisplayDate = DateTime.Now;
            dateNascita.DisplayDate = DateTime.Now;
            txtID.Clear();
            txtNCivico.Clear();
            currentUsr = null;
            txtLeftEntrance.Text = 0.ToString();
            listAccessi.Items.Clear();
        }

        protected void UpdateFields()
        {
            try
            {
                DBQueries db = new DBQueries("localhost", "root", "", "mypalestra");
                var user = db.GetCliente(int.Parse(currentUsr[0]));
                if (user != null)
                {
                    txtID.Text = currentUsr[0];
                    txtName.Text = user.Nome;
                    txtCognome.Text = user.Cognome;
                    txtEmail.Text = user.Email;
                    txtNtelefono.Text = user.Telefono;
                    txtCitta.Text = user.Citta;
                    txtIndirizzo.Text = $"{user.Indirizzo}";
                    txtNCivico.Text = $"{user.NCivico}";
                    dateNascita.SelectedDate = user.DataNascita;//.ToString("dd/MM/yyyy");
                    dateRegistrazione.SelectedDate = user.DataRegistrazione;//.ToString("dd/MM/yyyy");
                    txtLeftEntrance.Text = user.Entrate.ToString();
                    foreach (Accesso acc in db.GetAccessi(int.Parse(currentUsr[0])))
                    {
                        listAccessi.Items.Add($"{acc.Data.ToString("G")} {acc.Nome} {acc.Cognome}");
                    }
                }

            }
            catch (IndexOutOfRangeException) {
                Console.WriteLine("Eccezione in MainWindow.xaml -> Update fields");
            }
        }

        protected void SaveModify()
        {
            
            if (!isNewUser)
            {
                string messageBoxText = "Sei sicuro di effettuare le modifiche?";
                string caption = "Applicare le modifiche";
                MessageBoxButton button = MessageBoxButton.YesNo;
                MessageBoxImage icon = MessageBoxImage.Warning;
                MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        DBQueries db = new DBQueries();
                        Cliente cliente = new Cliente(int.Parse(currentUsr[0]), int.Parse(txtID.Text), txtName.Text, txtCognome.Text, txtEmail.Text, txtNtelefono.Text, txtIndirizzo.Text, txtNCivico.Text, txtCitta.Text, dateRegistrazione.SelectedDate.Value, dateNascita.SelectedDate.Value, int.Parse(txtLeftEntrance.Text));
                        db.UpdateCliente(cliente);
                        controller.AdminWriteToCard(txtID.Text, txtLeftEntrance.Text);
                        break;
                    case MessageBoxResult.No:
                        ClearFields();
                        ModifyTxtBox(false);
                        break;
                }
            }
            else
            {
                if (txtID.Text.Length == 0)
                {
                    string messageBoxText = "Confermi la creazione dell'utente?";
                    string caption = "Creazione utente";
                    MessageBoxButton button = MessageBoxButton.YesNo;
                    MessageBoxImage icon = MessageBoxImage.Warning;
                    MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            DBQueries db = new DBQueries();
                            //Console.WriteLine("creo l'utente");
                            int tessera = db.CreateTessera(false, int.Parse(txtLeftEntrance.Text));
                            Cliente cliente = db.CreateCliente(tessera, txtName.Text, txtCognome.Text, txtEmail.Text, txtNtelefono.Text, txtIndirizzo.Text, txtNCivico.Text, txtCitta.Text, dateNascita.SelectedDate.Value, dateRegistrazione.SelectedDate.Value);
                            controller.AdminWriteToCard(cliente.ID.ToString());
                            ClearFields();
                            break;
                        case MessageBoxResult.No:
                            ClearFields();
                            ModifyTxtBox(false);
                            break;
                    }
                }
                else
                {
                    string messageBoxText = "Vuoi collegare la tessera all'utente?";
                    string caption = "Collegamento tessera";
                    MessageBoxButton button = MessageBoxButton.YesNo;
                    MessageBoxImage icon = MessageBoxImage.Warning;
                    MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);

                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            controller.AdminWriteToCard(txtID.Text);
                            ClearFields();
                            break;
                        case MessageBoxResult.No:
                            ClearFields();
                            ModifyTxtBox(false);
                            break;
                    }
                }
            }
            
        }

        protected void ModifyTxtBox(bool enabled)
        {
            txtName.IsEnabled = enabled;
            txtEmail.IsEnabled = enabled;
            txtNtelefono.IsEnabled = enabled;
            txtCognome.IsEnabled = enabled;
            txtCitta.IsEnabled = enabled;
            txtIndirizzo.IsEnabled = enabled;
            dateRegistrazione.IsEnabled = enabled;
            txtID.IsEnabled = enabled;
            txtLeftEntrance.IsEnabled = enabled;
            txtNCivico.IsEnabled = enabled;
            dateNascita.IsEnabled = enabled;
            if (enabled)
            {
                btnModifica.Content = "Salva";
            }
            else
            {
                btnModifica.Content = "Modifica";
                isNewUser = false;
            }
        }


        #endregion

        private void BtnStampa_Click(object sender, RoutedEventArgs e)
        {
            if (!Entrata.isOpened)
            {
                entrate = new Entrata(controller);
                entrate.Show();
                Entrata.isOpened = true;
            }
        }

        private void Corsi_Click(object sender, RoutedEventArgs e)
        {
            if (!WPFCorsi.isOpened)
            {
                Window windowCorsi = new WPFCorsi();
                windowCorsi.Activate();
            }

        }

        private void onClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            
        }

        private void WindowRendered(object sender, EventArgs e)
        {
            if (!Entrata.isOpened)
            {
                entrate = new Entrata(controller);
                entrate.Show();
                Entrata.isOpened = true;
            }
            /*
            Thread thread = new Thread(() => LoadEntrate(this.controller));
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();*/
        }

        private void AggiungiACorso_Click(object sender, RoutedEventArgs e)
        {
            if (!Partecipazione.isOpened)
            {
                Window windowPartecipazione = new Partecipazione(int.Parse(currentUsr[0]));
                windowPartecipazione.Activate();
            }
        }
    }
}
