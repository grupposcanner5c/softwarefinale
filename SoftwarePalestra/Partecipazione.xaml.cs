﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SoftwarePalestra
{
    /// <summary>
    /// Logica di interazione per Partecipazione.xaml
    /// </summary>
    public partial class Partecipazione : Window
    {
        public static bool isOpened = false;
        public int ident = 0;
        DBQueries database = new DBQueries("localhost", "root", null, "mypalestra");
        public Partecipazione(int id)
        {
            try{
            ident = id;
            }catch(NullReferenceException){
                Console.WriteLine("Seleziona un utente");
            }
            InitializeComponent();
            this.Show();
            isOpened = true;
        }

        private void Iscrivi_Click(object sender, RoutedEventArgs e)
        {
            database.CreatePartecipazione(database.GetIDCorso(SelezionaCorso.Text),ident);
        }

        private void Lezioni_Loaded(object sender, RoutedEventArgs e)
        {
            List<Corso> corsi = database.GetCorso();
            foreach (Corso corso in corsi)
            {
                SelezionaCorso.Items.Add(corso.Nome);
            }
        }
    }
}
