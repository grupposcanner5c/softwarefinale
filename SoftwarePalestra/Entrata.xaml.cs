﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Windows.Threading;
using System.Globalization;

namespace SoftwarePalestra
{
    /// <summary>
    /// Logica di interazione per Entrata.xaml
    /// </summary>
    public partial class Entrata : Window
    {
        MSRController controller;

        DBQueries queries = new DBQueries();

        public static bool isOpened = false;

        internal string Status
        {
            get { return label_user_name.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { label_user_name.Content = value; })); }
        }

        public Entrata(MSRController controller)
        {
            InitializeComponent();
            this.Show();
            label_time.Content = "DATA ATTUALE";
            label_welcome.Content = "BENVENUTO";
            label_entrance.Content = "Entrate rimaste:";
            startclock();
            Console.WriteLine("Costruttore");
            this.controller = controller;
            //Console.WriteLine(this.controller == null ? "null" : "not null");
        }




        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            label_datetime.Content = DateTime.Now.ToString();
        }
        private void pictureBox_uk_Click(object sender, MouseButtonEventArgs e)
        {
            label_time.Content = "CURRENT DATE";
            label_welcome.Content = "WELCOME";
            label_entrance.Content = "Left entrance:";
        }

        private void pictureBox_italy_Click(object sender, MouseButtonEventArgs e)
        {
            label_time.Content = "DATA ATTUALE";
            label_welcome.Content = "BENVENUTO";
            label_entrance.Content = "Entrate rimaste:";
        }

        private void Window_Rendered(object sender, EventArgs e)
        {
            Console.WriteLine("WindowRendered");
            Task.Factory.StartNew(() => startScanner());
        }

        private void startScanner()
        {
            DBQueries queries = new DBQueries("127.0.0.1", "root", "", "mypalestra");
            bool interrupt = false;
            
            while (!interrupt)
            {
                try
                {
                    string[] tmp = controller.ReadFromCard();
                    if (tmp.Length > 0)
                    {
                        string id = tmp[0];
                        var user = queries.GetCliente(int.Parse(id));
                        bool isAdmitted = queries.ReduceEntrate(int.Parse(id));
                        if (user != null)
                        {
                            queries.AddtoRegistro(int.Parse(id));
                            Dispatcher.BeginInvoke(new Action(() =>
                            {
                                label_user_name.Content = user.Nome;
                                if (isAdmitted)
                                {
                                    label_entrance_left.Content = user.Entrate;
                                }
                                else
                                {
                                    label_entrance_left.Content = "Entrate residue insufficienti";
                                }
                            }), DispatcherPriority.Background);
                        }
                    }
                }
                catch (Exception e)
                {  
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Eccezione in entrata.xaml -> StartScanner");
                    //Console.WriteLine(id);
                }
            }
        }

        struct ParametriScanner
        {
            public MSRController controller;
            public DBQueries queries;
            public Window username;
            public Label entranceLeft;

            public ParametriScanner(MSRController controller, DBQueries queries, Window username, Label entranceLeft)
            {
                this.controller = controller;
                this.queries = queries;
                this.username = username;
                this.entranceLeft = entranceLeft;
            }
        }

        public void ReloadCorsi()
        {
            ListView_Corsi_Loaded(null, null);
        }

        private void ListView_Corsi_Loaded(object sender, RoutedEventArgs e)
        {
            ListView_Corsi.Items.Clear();
            List<Lezione> lezioni = queries.GetLezioni();
            foreach (Lezione v in lezioni)
            {
                if (v.GetGiorno().Equals(DateTime.Now.ToString("dd/MM/yyyy"))) 
                {
                    DateTime dateTime = Convert.ToDateTime(v.Inizio);
                    Console.WriteLine(dateTime.Day);
                    if (dateTime.CompareTo(DateTime.Now) == 1)
                    {
                        ListView_Corsi.Items.Add(v.GetNome() + " - " + v.giorno.ToString("dd/MM") + " - " + v.Inizio + " - " + v.Durata);
                    }
                    
                }
            } 
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            isOpened = false;
        }
    }
}
    

